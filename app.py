from flask import Flask,render_template,jsonify,request
from form.my_form import LoginForm, MyForm
from flask_restful import Resource,Api,reqparse
from flask_jwt_extended import JWTManager,jwt_required,get_jwt_identity,create_access_token
from user import User 
from flask_sqlalchemy import SQLAlchemy
from models.item import ItemModel

app = Flask(__name__)
api = Api(app)
# Setup the Flask-JWT-Extended extension
app.config["JWT_SECRET_KEY"] = "miles-secret"  # Change this!
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
jwt = JWTManager(app)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

@app.route('/login',methods=['GET','POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        return "<p>Logined</p>"
    else:
        print('get methods')
    return render_template('bootstrap-flask.html', form=form)


@app.route('/mymy',methods=['GET','POST'])
def my_form():
    form = MyForm();
    if form.validate_on_submit():
        return 'hello'
    return render_template('my_form.html', form=form)


@app.route("/login", methods=["POST"])
def login():
    username = request.json.get("username", None)
    password = request.json.get("password", None)
    if username != "test" or password != "test":
        return jsonify({"msg": "Bad username or password"}), 401

    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token)



""" api.add_resource(Item,'/item/<string:name>')
api.add_resource(ItemList,'/items') """
api.add_resource(User,'/user')

if __name__ == "__main__":
    app.run()
