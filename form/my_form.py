from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, PasswordField
from wtforms.fields.simple import SubmitField
from wtforms.validators import DataRequired, Length, Optional


class MyForm(FlaskForm):
    name = StringField('name', validators=[DataRequired(),Length(min=3)], 
        render_kw={"placeholder":"输入用户名","required": False,"minlength":False}
    )
    submit = SubmitField()


class LoginForm(FlaskForm):
    username = StringField(
        '用户名', validators=[DataRequired(message=u'用户名不能为空')])
    password = PasswordField(
        '密码', validators=[DataRequired(), Length(min=6, max=10, message=u'长度最小6最长10')])
    reminder_me = BooleanField('记住我', validators=[Optional()])
    submit = SubmitField('Login')
