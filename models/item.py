from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()

class ItemModel(db.Model):

    __tablename__ = 't_item'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    price = db.Column(db.Integer)


    def __init__(self,name,price) -> None:
        self.name = name
        self.price = price

    def find_by_name(name):
        print(name)
        item = ItemModel.query.filter_by(name=name).first()
        for column in item.__table__.columns:
            print(column.name)
        return item

    def __repr__(self) -> str:
        for k in self.__dict__:
            print(k)
        return 'test'